import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CartPage } from './cart';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    CartPage,
  ],
  imports: [
    IonicPageModule.forChild(CartPage),
    LottieAnimationViewModule
    
  ],
})
export class CartPageModule {}
