import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { PopupPage } from '../popup/popup';
import { MenuPage } from '../menu/menu';

/**
 * Generated class for the CheckoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-checkout',
  templateUrl: 'checkout.html',
})
export class CheckoutPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CheckoutPage');
  }

  showPopup() {
    let profileModal = this.modalCtrl.create(PopupPage);
    profileModal.present();

    setTimeout(() => {
      profileModal.dismiss();
      this.navCtrl.setRoot(MenuPage);
  }, 1500);
  }
 

}
