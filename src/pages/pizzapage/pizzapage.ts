import { CartPage } from './../cart/cart';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PizzapagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pizzapage',
  templateUrl: 'pizzapage.html',
})
export class PizzapagePage {
  tquantity:number =0;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PizzapagePage');
  }

  deleteFromCart()
  {
    if(this.tquantity!=0)
    {
      this.tquantity--;
    }

  }

  addToCart()
  {
    this.tquantity++;

  }

  goCart()
  {
    this.navCtrl.push(CartPage);
  }

}
