import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { PizzamenuPage } from '../pizzamenu/pizzamenu';
import { CartPage } from '../cart/cart';

/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public menuCtrl: MenuController) {
    this.menuCtrl.swipeEnable;
    this.menuCtrl.enable(true);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuPage');
  }

  goPizza()
  {
    this.navCtrl.push(PizzamenuPage);
  }

  goCart()
  {
    this.navCtrl.push(CartPage);
  }

  openMenu() {
    this.menuCtrl.open();
  }

}
