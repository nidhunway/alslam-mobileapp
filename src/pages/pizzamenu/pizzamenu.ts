import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PizzapagePage } from '../pizzapage/pizzapage';
import { CartPage } from '../cart/cart';

/**
 * Generated class for the PizzamenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pizzamenu',
  templateUrl: 'pizzamenu.html',
})
export class PizzamenuPage {
  tquantity:number =0;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PizzamenuPage');
  }
  deleteFromCart()
  {
    if(this.tquantity!=0)
    {
      this.tquantity--;
    }

  }

  addToCart()
  {
    this.tquantity++;

  }

  goPizzapage()
  {
    this.navCtrl.push(PizzapagePage);
  }
  goCart()
  {
    this.navCtrl.push(CartPage);
  }
}
