import { PopupPageModule } from './../pages/popup/popup.module';
import { CheckoutPageModule } from './../pages/checkout/checkout.module';
import { PizzapagePageModule } from './../pages/pizzapage/pizzapage.module';
import { MenuPageModule } from './../pages/menu/menu.module';
import { RegisterPageModule } from './../pages/register/register.module';
import { LoginPageModule } from './../pages/login/login.module';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { PizzamenuPageModule } from '../pages/pizzamenu/pizzamenu.module';
import { CartPageModule } from '../pages/cart/cart.module';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    LoginPageModule,
    RegisterPageModule,
    MenuPageModule,
    PizzamenuPageModule,
    PizzapagePageModule,
    CartPageModule,
    CheckoutPageModule,
    PopupPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
